# twitter_sentiment
A Twitter sentiment analysis tool using the Twitter API and the TextBlob library. 

## Dependencies

1. tweepy 
2. textblob 

Run the below command in terminal:

```
pip3 install -r requirements.txt
```

## Usage

Add Twitter API keys and access tokens to `authentication.py`:

```
consumer_key= ''
consumer_secret= ''
access_token= ''
access_token_secret= ''
```

Run the below script in terminal:

```
python3 twitter_sentiment.py
```

Enter topic for sentiment analysis. Result is Positive (greater than 0.1), Negative (less than -0.1), or Neutral (between -0.1 and 0.1).  The farther away from 0, the stronger the sentiment. 
