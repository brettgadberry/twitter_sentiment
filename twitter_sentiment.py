import tweepy
from textblob import TextBlob
import csv
import sys

from authentication import consumer_key, consumer_secret, access_token, access_token_secret

# authenticate with consumer keys and access token
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

# initialize wirting to CSV file
tweetFile = open('tweet.csv','w')
tweetWriter = csv.writer(tweetFile)
tweetWriter.writerow(["tweet","result","polarity"])

# query string
topic = input('Enter topic for Twitter sentiment analysis: ')

# catch errors
try: 
    public_tweets = api.search(topic, count=100, lang="en")
except tweepy.TweepError:
    print('Invalid credentials')
    sys.exit()

# perform sentiment analysis on tweets and wite to CSV file
polarityList = []
for tweet in public_tweets:
    tweet = tweet.text
    analysis = TextBlob(tweet)

    subjectivity = analysis.sentiment.subjectivity
    if subjectivity < 0.1:
        continue

    polarity = analysis.sentiment.polarity
    polarityList.append(polarity)
    result = ''
    if polarity >= 0.1:
        result = 'Positive'
    elif polarity <= -0.1:
        result = 'Negative'
    else:
        result = 'Neutral'

    tweetWriter.writerow([tweet,result,polarity])

# sentiment analysis on all tweets
PolarityAvg = sum(polarityList) / len(polarityList)
final_result = ''
if PolarityAvg > 0.1:
    final_result = 'Positive'
elif PolarityAvg < -0.1:
    final_result = 'Negative'
else:
    final_result = 'Neutral'

# print final result
print('The Twittersphere is %s (%s) on %s' % (final_result, PolarityAvg, topic))
    





